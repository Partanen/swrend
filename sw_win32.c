#include <windows.h>

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")
#pragma comment(lib, "Winmm.lib")

struct sw_framebuffer_data_t
{
    BITMAPINFO  bitmap_info;
    void        *mem;
    int         num_alloc_pixels;
};

struct sw_window_data_t
{
    HWND    wnd;
    HDC     dc;
};

static LRESULT CALLBACK
_sw_win32_window_proc(HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam);

static void
_sw_win32_on_raw_keyboard(RAWKEYBOARD *rkb, sw_window_t *win);

static void
_sw_win32_on_raw_mouse(RAWMOUSE *rm, sw_window_t *win);

int
sw_plat_init_video(void)
    {return 0;}

int
sw_plat_window_init_internal(sw_window_t *win, const char *name, int x, int y,
    int w, int h, int flags)
{
    int ret = 0;
    sw_window_data_t *data = win->data;

    HWND        wnd             = 0;
    LPCSTR      win_class_name  = "sw window class";
    WNDCLASSEX  wc              = {0};

    wc.style            = CS_HREDRAW | CS_VREDRAW;
    wc.cbSize           = sizeof(WNDCLASSEX);
    wc.lpfnWndProc      = _sw_win32_window_proc;
    wc.lpszClassName    = win_class_name;

    if (!RegisterClassEx(&wc)) {
        sw_set_error("Failed to register window class");
        ret = 1;
        goto fail;
    }

    wnd = CreateWindowExA(0, win_class_name, name,
        WS_OVERLAPPEDWINDOW | WS_VISIBLE, x, y, w, h, 0, 0, 0, win);

    if (!wnd) {
        sw_set_error("Window creation failed");
        ret = 2;
        goto fail;
    }

    data->dc = GetWindowDC(wnd);

    if (!data->dc) {
        sw_set_error("Failed to get window device context.\n");
        ret = 3;
        goto fail;
    }

    data->wnd = wnd;

    /* Register input devices */
    RAWINPUTDEVICE rid[2];

    /* Keyboard */
    rid[0].usUsagePage  = 0x01;
    rid[0].usUsage      = 0x06; /* Keyboard */
    rid[0].dwFlags      = 0x00;
    rid[0].hwndTarget   = wnd;

    /* Mouse */
    rid[1].usUsagePage  = 0x01;
    rid[1].usUsage      = 0x02; /* Mouse */
    rid[1].dwFlags      = 0x00;
    rid[1].hwndTarget   = wnd;

    RegisterRawInputDevices(rid, 2, sizeof(RAWINPUTDEVICE));

    ShowWindow(wnd, SW_SHOWNORMAL);
    return ret;

    fail:
        sw_free(win);
        /* Free hwnd */
        return ret;
}

void
sw_plat_destroy_window_data(sw_window_data_t *data)
    {(void)data;}

void
sw_pump_events_internal(void)
{
    MSG msg;
	while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

static LRESULT CALLBACK
_sw_win32_window_proc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    sw_event_t ev;
    switch (msg) {
	case WM_CLOSE:
        ev.type = SW_EVENT_QUIT;
        sw_post_window_event(&ev);
		break;
	case WM_DESTROY:
        ev.type = SW_EVENT_QUIT;
        sw_post_window_event(&ev);
		break;
    case WM_INPUT: {
        if (!_window)
            break;
        RAWINPUT buf;
        UINT sz = sizeof(RAWINPUT);
        GetRawInputData((HRAWINPUT)lparam, RID_INPUT, &buf, &sz,
            sizeof(RAWINPUTHEADER));
        sw_window_t *win = _window;
        if (buf.header.dwType == RIM_TYPEKEYBOARD)
            _sw_win32_on_raw_keyboard(&buf.data.keyboard, win);
        else if (buf.header.dwType == RIM_TYPEMOUSE)
            _sw_win32_on_raw_mouse(&buf.data.mouse, win);
    }
        break;
    case WM_SIZE:
    {
        if (!_window)
            break;
        sw_window_t *win = _window;
        RECT cr;
        GetClientRect(win->data->wnd, &cr);
        int fw = cr.right - cr.left;
        int fh = cr.bottom - cr.top;
        ev.type = SW_EVENT_WINDOW_SIZE_CHANGED;
        ev.window.w = fw; //LOWORD(lparam);
        ev.window.h = fh;//HIWORD(lparam);
        sw_post_window_event(&ev);
    }
        break;
    default:
        return DefWindowProc(hwnd, msg, wparam, lparam);
    }
    return 0;
}

int
sw_plat_framebuffer_data_init(sw_framebuffer_data_t *data, sw_window_t *win,
    int w, int h)
{
    data->bitmap_info.bmiHeader.biSize = sizeof(data->bitmap_info.bmiHeader);
    data->bitmap_info.bmiHeader.biWidth         = w;
    data->bitmap_info.bmiHeader.biHeight        = -h;
    data->bitmap_info.bmiHeader.biPlanes        = 1;
    data->bitmap_info.bmiHeader.biBitCount      = 32;
    data->bitmap_info.bmiHeader.biCompression   = BI_RGB;

    int num_alloc_pixels = w * h;

    data->mem = sw_calloc(num_alloc_pixels * 4);
    if (!data->mem) {
        sw_set_error(_sw_err_str_out_of_memory);
        return 1;
    }

    data->num_alloc_pixels = num_alloc_pixels;
    return 0;
}

void
sw_plat_destroy_framebuffer_data(sw_framebuffer_data_t *data)
    {sw_free(data->mem);}

void
sw_plat_update_framebuffer(sw_framebuffer_t *fb)
{
    sw_window_t *win = _window;

    RECT cr, wr;
    GetClientRect(win->data->wnd, &cr);
    GetWindowRect(win->data->wnd, &wr);

    int fw = cr.right - cr.left;
    int fh = cr.bottom - cr.top;

    int dx = (wr.right - wr.left) - cr.right;
    int dy = (wr.bottom - wr.top) - cr.bottom;

    StretchDIBits(win->data->dc, dx / 2, dy - dx / 2, fw, fh, 0, 0, fw, fh,
        fb->data->mem, &fb->data->bitmap_info, DIB_RGB_COLORS, SRCCOPY);
}

void *
sw_plat_get_framebuffer_data_pixels(sw_framebuffer_data_t *data)
    {return data->mem;}

int
sw_resize_framebuffer_data(sw_framebuffer_data_t *fb, int w, int h)
{
    int num_pixels = w * h;
    if (num_pixels > fb->num_alloc_pixels) {
        void *mem = sw_realloc(fb->mem, num_pixels * 4);
        if (!mem) {
            sw_set_error(_sw_err_str_out_of_memory);
            return 1;
        }
        fb->mem = mem;
    }
    fb->bitmap_info.bmiHeader.biWidth   = w;
    fb->bitmap_info.bmiHeader.biHeight  = -h;
    return 0;
}

long unsigned int
sw_plat_get_performance_frequency()
{
    LARGE_INTEGER ret;
    QueryPerformanceFrequency(&ret);
    return (long unsigned int)ret.QuadPart;
}

long unsigned int
sw_plat_get_performance_counter()
{
    LARGE_INTEGER num_ticks;
    QueryPerformanceCounter(&num_ticks);
    return (long unsigned int)num_ticks.QuadPart;
}

int
sw_plat_sleep_milliseconds(long unsigned int num_ms)
{
    Sleep((DWORD)num_ms);
    return 0;
}

int
sw_plat_init_time(void)
{
    if (timeBeginPeriod(1) != TIMERR_NOERROR)
        return 1;
    return 0;
}

void
sw_plat_quit_time(void)
{
    timeEndPeriod(1);
}

static void
_sw_win32_on_raw_keyboard(RAWKEYBOARD *rkb, sw_window_t *win)
{
}

static void
_sw_win32_on_raw_mouse(RAWMOUSE *rm, sw_window_t *win)
{
}
