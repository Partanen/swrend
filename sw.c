#include <string.h>
#include "sw.h"
#include "sw_internal.h"

static sw_window_t  *_window;
static int          _video_initialized;

#if _WIN32
    #include "sw_win32.c"
#elif __linux__
    #include "sw_linux.c"
#endif

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define SW_MAX_QUEUED_EVENTS 128

static const char *_sw_last_error = "No error";

static struct
{
    sw_event_t  items[SW_MAX_QUEUED_EVENTS];
    int         num;
    int         first;
} _events;

/* Common error strings */
const char *_sw_err_str_out_of_memory           = "Out of memory";
const char *_sw_err_str_invalid_parameter       = "Invalid parameter";
const char *_sw_err_str_failed_to_open_display  = "Failed to open display";
const char *_sw_err_str_video_uninitialized = "Video has not been initialized";

int
sw_init(int flags)
{
    int ret = 0;

    if (!flags)
        flags = SW_INIT_VIDEO;

    if (flags & SW_INIT_VIDEO && sw_init_video()) {
        ret = 1;
        goto err;
    }

    if (sw_init_time()) {
        ret = 2;
        goto err;
    }

    return ret;

    err:
        sw_video_free();
        return ret;
}

void
sw_quit()
{
    if (_window)
        sw_destroy_window(); 
}

int
sw_init_video(void)
{
    if (sw_video_initialized())
        return 0;
    if (sw_plat_init_video())
        return 1;
    _video_initialized = 1;
    return 0;
}

void
sw_video_free(void) {}

int
sw_init_time()
    {return sw_plat_init_time();}

int
sw_video_initialized(void)
    {return _video_initialized;}

void
sw_set_error(const char *err)
    {_sw_last_error = err;}

const char *
sw_get_error()
    {return _sw_last_error;}

int
sw_create_window(const char *name, int x, int y, int w, int h, int flags)
{
    static char window_buf[sizeof(sw_window_t)];
    static char window_data_buf[sizeof(sw_window_data_t)];

    if (!sw_video_initialized()) {
        sw_set_error(_sw_err_str_video_uninitialized);
        return 1;
    }

    if (_window) {
        sw_set_error("Only one window per app is currently supported");
        return 2;
    }

    if (!flags)
        flags |= SW_WINDOW_SOFTWARE;

    sw_window_t *win = (sw_window_t*)window_buf;
    win->data = (sw_window_data_t*)window_data_buf;

    if (sw_plat_window_init_internal(win, name, x , y, w, h, flags)) {
        sw_free(win);
        return 3;
    }

    if (flags & SW_WINDOW_SOFTWARE)
        if (!(win->framebuffer = sw_create_framebuffer(win, w, h))) {
            sw_destroy_window();
            return 4;
        }

    win->flags  = flags;
    win->w      = w;
    win->h      = h;
    _window     = win;
    return 0;
}

void
sw_destroy_window(void)
{
    if (!_window) {
        sw_set_error("No window has been created");
        return;
    }
    sw_plat_destroy_window_data(_window->data);
    if (_window->flags & SW_WINDOW_SOFTWARE)
        sw_destroy_framebuffer(_window->framebuffer);
    _window = 0;
}

void
sw_update_window()
{
    if (!(_window->flags & SW_WINDOW_SOFTWARE))
        return;
    sw_update_framebuffer(_window->framebuffer);
}

void
sw_get_window_dimensions(sw_window_t *win, int *ret_w, int *ret_h)
{
    *ret_w = win->w;
    *ret_h = win->h;
}

void
sw_pump_events(void)
    {sw_pump_events_internal();}

int
sw_poll_events(sw_event_t *ret_ev)
{
    if (!ret_ev) {
        sw_set_error(_sw_err_str_invalid_parameter);
        return 0;
    }
    sw_pump_events();
    if (!_events.num)
        return 0;
    *ret_ev = _events.items[_events.first];
    _events.first = (_events.first + 1) % SW_MAX_QUEUED_EVENTS;
    _events.num--;
    return 1;
}

int
sw_post_event(sw_event_t *ev)
{
    if (!ev) {
        sw_set_error(_sw_err_str_invalid_parameter);
        return 1;
    }
    if (_events.num == SW_MAX_QUEUED_EVENTS) {
        sw_set_error("Event queue full");
        return 2;
    }
    _events.items[(_events.first + _events.num++) % SW_MAX_QUEUED_EVENTS]= *ev;
    return 0;
}

int
sw_img_load(sw_img_t *img, const char *fp)
{
    if (!img) {
        sw_set_error(_sw_err_str_invalid_parameter);
        return 1;
    }
    img->data = stbi_load(fp, &img->w, &img->h, &img->bpp, 0);
    if (!img->data) {
        sw_set_error("Failed to load image");
        return 2;
    }
    img->pxfmt = img->bpp == 3 ? SW_IMG_RGB : SW_IMG_RGBA;
    return 0;
}

void
sw_img_free(sw_img_t *img)
{
    if (!img)
        return;
    stbi_image_free(img->data);
    memset(img, 0, sizeof(sw_img_t));
}

sw_framebuffer_t *
sw_create_framebuffer(sw_window_t *win, int w, int h)
{
    size_t fb_sz = sizeof(sw_framebuffer_t);
    fb_sz = fb_sz + 4 - (fb_sz % 4);

    sw_framebuffer_t *ret = sw_calloc(fb_sz + sizeof(sw_framebuffer_data_t));
    if (!ret) {
        sw_set_error(_sw_err_str_out_of_memory);
        return 0;
    }
    ret->data = (sw_framebuffer_data_t*)((char*)ret + fb_sz);
    if (sw_plat_framebuffer_data_init(ret->data, win, w, h)) {
        sw_free(ret);
        return 0;
    }
    ret->w          = w;
    ret->h          = h;
    ret->is_valid   = 1;
    return ret;
}

void
sw_destroy_framebuffer(sw_framebuffer_t *fb)
{
    if (!fb)
        return;
    sw_plat_destroy_framebuffer_data(fb->data),
    sw_free(fb);
}

void
sw_update_framebuffer(sw_framebuffer_t *fb)
{
    if (!fb->is_valid)
        sw_resize_framebuffer(fb, _window->w, _window->h);
    sw_plat_update_framebuffer(fb);
}

void *
sw_get_window_framebuffer_pixels(int *ret_w, int *ret_h)
{
    if (!_window)
        return 0;
    if (!_window->flags & SW_WINDOW_SOFTWARE)
        return 0;
    if (ret_w)
        *ret_w = _window->framebuffer->w;
    if (ret_h)
        *ret_h = _window->framebuffer->h;
    return sw_plat_get_framebuffer_data_pixels(_window->framebuffer->data);
}

long unsigned int
sw_get_performance_frequency()
    {return sw_plat_get_performance_frequency();}

long unsigned int
sw_get_performance_counter()
    {return sw_plat_get_performance_counter();}

int
sw_sleep_milliseconds(long unsigned int num_ms)
    {return sw_plat_sleep_milliseconds(num_ms);}

int
sw_resize_framebuffer(sw_framebuffer_t *fb, int w, int h)
{
    if (fb->w == w && fb->h == h)
        return 0;
    int r = sw_resize_framebuffer_data(fb->data, w, h);
    if (!r) {
        fb->w = w;
        fb->h = h;
    }
    fb->is_valid = 1;
    return r;
}

int
sw_post_window_event(sw_event_t *ev)
{
    int ret = sw_post_event(ev);
    if (ret)
        return ret;
    if (ev->type != SW_EVENT_WINDOW_SIZE_CHANGED)
        return ret;
    if (!_window)
        return ret;
    if (ev->window.w == _window->w && ev->window.h == _window->h)
        return 0;
    _window->w  = ev->window.w;
    _window->h  = ev->window.h;
    if (_window->framebuffer)
        _window->framebuffer->is_valid = 0;
    return ret;
}
