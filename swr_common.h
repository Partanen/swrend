#include <stdio.h>

#if defined(_DEBUG)
    #define swr_assert(cond_) \
        ((cond_) ? (void)0 : \
        ((void)printf("Assertion failed: %s, %s, line %d\n", __FILE__, \
        __func__, __LINE__), (void)fflush(stdout), (void)(*(int*)0 = 0)))
#else
    #define swr_assert(condition) ((void)0)
#endif
