#ifndef SW_INTERNAL_H
#define SW_INTERNAL_H
#include <stdlib.h>

#if defined(_DEBUG)
    #include <stdio.h>
    #define sw_debug_printf(fmt, ...) ((void)printf(fmt, ##__VA_ARGS__))
    #define sw_debug_printff(fmt, ...) \
        ((void)printf("%s: " fmt, __func__, ##__VA_ARGS__))
#else
    #define sw_debug_printf(fmt, ...) ((void)0)
    #define sw_debug_printff(fmt, ...) ((void)0)
#endif

typedef struct sw_framebuffer_data_t    sw_framebuffer_data_t;
typedef struct sw_framebuffer_t         sw_framebuffer_t;
typedef struct sw_window_data_t         sw_window_data_t;
const char *_sw_err_str_out_of_memory;
const char *_sw_err_str_invalid_parameter;
const char *_sw_err_str_failed_to_open_display;
const char *_sw_err_str_video_uninitialized;
#define sw_malloc       malloc
#define sw_calloc(sz_)  calloc(1, (sz_))
#define sw_realloc      realloc
#define sw_free         free

struct sw_window_t
{
    int                 flags;
    int                 w, h;
    sw_window_data_t    *data; /* System specific data */
    sw_framebuffer_t    *framebuffer;
};

struct sw_framebuffer_t
{
    int                     is_valid;
    int                     w, h;
    sw_framebuffer_data_t   *data;
};



sw_framebuffer_t *
sw_create_framebuffer(sw_window_t *win, int w, int h);

void
sw_destroy_framebuffer(sw_framebuffer_t *fb);


void
sw_update_framebuffer(sw_framebuffer_t *fb);

int
sw_resize_framebuffer(sw_framebuffer_t *fb, int w, int h);

int
sw_resize_framebuffer_data(sw_framebuffer_data_t *fb, int w, int h);

void
sw_pump_events_internal(void);

int
sw_post_window_event(sw_event_t *ev);



/*-- Functions defined in platform-specific files: --*/

int
sw_plat_init_video(void);

int
sw_plat_window_init_internal(sw_window_t *win, const char *name, int x, int y,
    int w, int h, int flags);

void
sw_plat_destroy_window_data(sw_window_data_t *data);

int
sw_plat_framebuffer_data_init(sw_framebuffer_data_t *data, sw_window_t *win,
    int w, int h);

void
sw_plat_destroy_framebuffer_data(sw_framebuffer_data_t *data);

void
sw_plat_update_framebuffer(sw_framebuffer_t *fb);

void *
sw_plat_get_framebuffer_data_pixels(sw_framebuffer_data_t *data);


long unsigned int
sw_plat_get_performance_frequency();

long unsigned int
sw_plat_get_performance_counter();

int
sw_plat_sleep_milliseconds(long unsigned int num_ms);

int
sw_plat_init_time();

void
sw_plat_quit_time();

#endif
