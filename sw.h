#ifndef SW_H
#define SW_H

typedef struct sw_window_event_t    sw_window_event_t;
typedef struct sw_key_event_t       sw_key_event_t;
typedef union sw_event_t            sw_event_t;
typedef struct sw_window_t          sw_window_t;
typedef struct sw_img_t             sw_img_t;

enum sw_event_type {
    SW_EVENT_QUIT,
    SW_EVENT_WINDOW_SIZE_CHANGED,
    SW_EVENT_KEY_DOWN
};

enum sw_init_flag {
    SW_INIT_VIDEO = (1 << 0)
};

enum sw_window_flag {
    SW_WINDOW_SOFTWARE = (1 << 0)
};

enum sw_img_pixel_format {
    SW_IMG_RGB,
    SW_IMG_RGBA
};

enum sw_keycodes {
    SW_KEY_BACK,
    SW_KEY_TAB,
    SW_KEY_RETURN,
    SW_KEY_PAUSE,
    SW_KEY_CAPSLOCK,
    SW_KEY_ESCAPE,
    SW_KEY_SPACE,
    SW_KEY_PAGEUP,
    SW_KEY_PAGEDOWN,
    SW_KEY_END,
    SW_KEY_HOME,
    SW_KEY_LEFT,
    SW_KEY_UP,
    SW_KEY_RIGHT,
    SW_KEY_DOWN,
    SW_KEY_SELECT,
    SW_KEY_INSERT,
    SW_KEY_DELETE,
    SW_KEY_HELP,
    SW_KEY_NUM_0,
    SW_KEY_NUM_1,
    SW_KEY_NUM_2,
    SW_KEY_NUM_3,
    SW_KEY_NUM_4,
    SW_KEY_NUM_5,
    SW_KEY_NUM_6,
    SW_KEY_NUM_7,
    SW_KEY_NUM_8,
    SW_KEY_NUM_9,
    SW_KEY_A,
    SW_KEY_B,
    SW_KEY_C,
    SW_KEY_D,
    SW_KEY_E,
    SW_KEY_F,
    SW_KEY_G,
    SW_KEY_H,
    SW_KEY_I,
    SW_KEY_J,
    SW_KEY_K,
    SW_KEY_L,
    SW_KEY_M,
    SW_KEY_N,
    SW_KEY_O,
    SW_KEY_P,
    SW_KEY_Q,
    SW_KEY_R,
    SW_KEY_S,
    SW_KEY_T,
    SW_KEY_U,
    SW_KEY_V,
    SW_KEY_W,
    SW_KEY_X,
    SW_KEY_Y,
    SW_KEY_Z,
    SW_KEY_NUMPAD_0,
    SW_KEY_NUMPAD_1,
    SW_KEY_NUMPAD_2,
    SW_KEY_NUMPAD_3,
    SW_KEY_NUMPAD_4,
    SW_KEY_NUMPAD_5,
    SW_KEY_NUMPAD_6,
    SW_KEY_NUMPAD_7,
    SW_KEY_NUMPAD_8,
    SW_KEY_NUMPAD_9,
    SW_KEY_NUMPAD_MULTIPLY,
    SW_KEY_NUMPAD_ADD,
    SW_KEY_NUMPAD_RETURN,
    SW_KEY_NUMPAD_SUBTRACT,
    SW_KEY_NUMPAD_DECIMAL,
    SW_KEY_NUMPAD_DIVIDE,
    SW_KEY_F1,
    SW_KEY_F2,
    SW_KEY_F3,
    SW_KEY_F4,
    SW_KEY_F5,
    SW_KEY_F6,
    SW_KEY_F7,
    SW_KEY_F8,
    SW_KEY_F9,
    SW_KEY_F10,
    SW_KEY_F11,
    SW_KEY_F12,
    SW_KEY_F13,
    SW_KEY_F14,
    SW_KEY_F15,
    SW_KEY_F16,
    SW_KEY_NUMLOCK,
    SW_KEY_SCROLL,
    SW_KEY_LEFT_SHIFT,
    SW_KEY_RIGHT_SHIFT,
    SW_KEY_LEFT_CONTROL,
    SW_KEY_RIGHT_CONTROL,
    SW_KEY_LEFT_ALT,
    SW_KEY_RIGHT_ALT,
    SW_KEY_UNKNOWN
};

struct sw_window_event_t {
    int type;
    int w, h;
};

struct sw_key_event_t {
    int type;
    int keycode;
    int is_repeat;
};

union sw_event_t {
    int                 type;
    sw_window_event_t   window;
    sw_key_event_t      key;
};

struct sw_img_t {
    void    *data;
    int     w, h;
    int     bpp;
    int     pxfmt;
};

int
sw_init(int flags);

void
sw_quit();

int
sw_init_video();

void
sw_video_free();

int
sw_init_time();

int
sw_video_initialized();

void
sw_set_error(const char *err);

const char *
sw_get_error(void);

int
sw_create_window(const char *name, int x, int y, int w, int h, int flags);
/* Each application may only have a single window. If the window already exists,
 * this function will do nothing.  */

void
sw_destroy_window(void);

void
sw_update_window();

void
sw_get_window_dimensions(sw_window_t *win, int *ret_w, int *ret_h);

void
sw_pump_events();

int
sw_poll_events(sw_event_t *ret_ev);

int
sw_post_event(sw_event_t *ev);

int
sw_img_load(sw_img_t *img, const char *fp);

void
sw_img_free(sw_img_t *img);

void *
sw_get_window_framebuffer_pixels(int *ret_w, int *ret_h);
/* ret_w and ret_h can be null */

long unsigned int
sw_get_performance_frequency();

long unsigned int
sw_get_performance_counter();

int
sw_sleep_milliseconds(long unsigned int num_ms);

#endif /* SW_H */
