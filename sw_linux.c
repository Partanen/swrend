#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>
#include <X11/keysym.h>
#include <time.h>

#define SW_X11_FRAMEBUFFER_MIN_ALLOC_PIXELS (2048 * 2048)

struct sw_framebuffer_data_t {
    XImage          *ximage;
    void            *mem;
    GC              gc;
    int             num_alloc_pixels;
    XVisualInfo     *xvi;
};

struct sw_window_data_t {
    int                 flags;
    Window              xwin;
    XVisualInfo         xvi;
    Atom                wm_delete_msg;
};

static Display *_xdisplay;

static struct {
    long unsigned int sec;
    long unsigned int msec;
} _program_start_time;

int
sw_plat_init_video(void)
{
    _xdisplay = XOpenDisplay(0);
    if (!_xdisplay) {
        sw_set_error(_sw_err_str_failed_to_open_display);
        return 1;
    }
    return 0;
}

int
sw_plat_window_init_internal(sw_window_t *win, const char *name, int x, int y,
    int w, int h, int flags)
{
    sw_window_data_t *data = win->data;

    int screen  = DefaultScreen(_xdisplay);

    if (!XMatchVisualInfo(_xdisplay, screen, 32, TrueColor, &data->xvi)) {
        sw_set_error("XMatchVisualInfo() failed");
        return 1;
    }

    XSetWindowAttributes swa;
    swa.colormap            = XCreateColormap(_xdisplay,
        RootWindow(_xdisplay, screen), data->xvi.visual, AllocNone);
    swa.background_pixmap   = None;
    swa.background_pixel    = 0;
    swa.border_pixel        = 0;
    swa.event_mask          = StructureNotifyMask;
    swa.bit_gravity         = NorthWestGravity;
    swa.win_gravity         = NorthWestGravity;

    data->xwin = XCreateWindow(_xdisplay, RootWindow(_xdisplay, screen),
        x, y, w, h, 0, data->xvi.depth, InputOutput,
        data->xvi.visual, CWColormap | CWBorderPixel | CWEventMask |
        CWBitGravity | CWWinGravity, &swa);

    if (!data->xwin) {
        sw_set_error("Failed to create X window");
        return 2;
    }

    data->wm_delete_msg = XInternAtom(_xdisplay, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(_xdisplay, data->xwin, &data->wm_delete_msg, 1);

    XSelectInput(_xdisplay, data->xwin, ExposureMask | ButtonPressMask |
        ButtonReleaseMask | KeyPressMask | KeyReleaseMask |
        StructureNotifyMask);
    XStoreName(_xdisplay, data->xwin, name);
    XMapWindow(_xdisplay, data->xwin);
    return 0;
}

void
sw_plat_destroy_window_data(sw_window_data_t *data)
{
    XDestroyWindow(_xdisplay, data->xwin);
    XCloseDisplay(_xdisplay);
}

static int _x11_key_to_sw_key(unsigned int keycode)
{
    switch (keycode)
    {
    case XK_BackSpace:      return SW_KEY_BACK;
    case XK_Tab:            return SW_KEY_TAB;
    case XK_Return:         return SW_KEY_RETURN;
    case XK_Pause:          return SW_KEY_PAUSE;
    case XK_Caps_Lock:      return SW_KEY_CAPSLOCK;
    case XK_Escape:         return SW_KEY_ESCAPE;
    case XK_space:          return SW_KEY_SPACE;
    case XK_Page_Up:        return SW_KEY_PAGEUP;
    case XK_Page_Down:      return SW_KEY_PAGEDOWN;
    case XK_End:            return SW_KEY_END;
    case XK_Home:           return SW_KEY_HOME;
    case XK_Left:           return SW_KEY_LEFT;
    case XK_Up:             return SW_KEY_UP;
    case XK_Right:          return SW_KEY_RIGHT;
    case XK_Down:           return SW_KEY_DOWN;
    case XK_Select:         return SW_KEY_SELECT;
    case XK_Insert:         return SW_KEY_INSERT;
    case XK_Delete:         return SW_KEY_DELETE;
    case XK_Help:           return SW_KEY_HELP;
    case XK_0:              return SW_KEY_NUM_0;
    case XK_1:              return SW_KEY_NUM_1;
    case XK_2:              return SW_KEY_NUM_2;
    case XK_3:              return SW_KEY_NUM_3;
    case XK_4:              return SW_KEY_NUM_4;
    case XK_5:              return SW_KEY_NUM_5;
    case XK_6:              return SW_KEY_NUM_6;
    case XK_7:              return SW_KEY_NUM_7;
    case XK_8:              return SW_KEY_NUM_8;
    case XK_9:              return SW_KEY_NUM_9;
    case XK_a:              return SW_KEY_A;
    case XK_b:              return SW_KEY_B;
    case XK_c:              return SW_KEY_C;
    case XK_d:              return SW_KEY_D;
    case XK_e:              return SW_KEY_E;
    case XK_f:              return SW_KEY_F;
    case XK_g:              return SW_KEY_G;
    case XK_h:              return SW_KEY_H;
    case XK_i:              return SW_KEY_I;
    case XK_j:              return SW_KEY_J;
    case XK_k:              return SW_KEY_K;
    case XK_l:              return SW_KEY_L;
    case XK_m:              return SW_KEY_M;
    case XK_n:              return SW_KEY_N;
    case XK_o:              return SW_KEY_O;
    case XK_p:              return SW_KEY_P;
    case XK_q:              return SW_KEY_Q;
    case XK_r:              return SW_KEY_R;
    case XK_s:              return SW_KEY_S;
    case XK_t:              return SW_KEY_T;
    case XK_u:              return SW_KEY_U;
    case XK_v:              return SW_KEY_V;
    case XK_w:              return SW_KEY_W;
    case XK_x:              return SW_KEY_X;
    case XK_y:              return SW_KEY_Y;
    case XK_z:              return SW_KEY_Z;
    case XK_KP_0:           return SW_KEY_NUMPAD_0;
    case XK_KP_1:           return SW_KEY_NUMPAD_1;
    case XK_KP_2:           return SW_KEY_NUMPAD_2;
    case XK_KP_3:           return SW_KEY_NUMPAD_3;
    case XK_KP_4:           return SW_KEY_NUMPAD_4;
    case XK_KP_5:           return SW_KEY_NUMPAD_5;
    case XK_KP_6:           return SW_KEY_NUMPAD_6;
    case XK_KP_7:           return SW_KEY_NUMPAD_7;
    case XK_KP_8:           return SW_KEY_NUMPAD_8;
    case XK_KP_9:           return SW_KEY_NUMPAD_9;
    case XK_KP_Multiply:    return SW_KEY_NUMPAD_MULTIPLY;
    case XK_KP_Add:         return SW_KEY_NUMPAD_ADD;
    case XK_KP_Enter:       return SW_KEY_NUMPAD_RETURN;
    case XK_KP_Subtract:    return SW_KEY_NUMPAD_SUBTRACT;
    case XK_KP_Decimal:     return SW_KEY_NUMPAD_DECIMAL;
    case XK_KP_Divide:      return SW_KEY_NUMPAD_DIVIDE;
    case XK_F1:             return SW_KEY_F1;
    case XK_F2:             return SW_KEY_F2;
    case XK_F3:             return SW_KEY_F3;
    case XK_F4:             return SW_KEY_F4;
    case XK_F5:             return SW_KEY_F5;
    case XK_F6:             return SW_KEY_F6;
    case XK_F7:             return SW_KEY_F7;
    case XK_F8:             return SW_KEY_F8;
    case XK_F9:             return SW_KEY_F9;
    case XK_F10:            return SW_KEY_F10;
    case XK_F11:            return SW_KEY_F11;
    case XK_F12:            return SW_KEY_F12;
    case XK_F13:            return SW_KEY_F13;
    case XK_F14:            return SW_KEY_F14;
    case XK_F15:            return SW_KEY_F15;
    case XK_F16:            return SW_KEY_F16;
    case XK_Num_Lock:       return SW_KEY_NUMLOCK;
    case XK_Scroll_Lock:    return SW_KEY_SCROLL;
    case XK_Shift_L:        return SW_KEY_LEFT_SHIFT;
    case XK_Shift_R:        return SW_KEY_RIGHT_SHIFT;
    case XK_Control_L:      return SW_KEY_LEFT_CONTROL;
    case XK_Control_R:      return SW_KEY_RIGHT_CONTROL;
    case XK_Alt_L:          return SW_KEY_LEFT_ALT;
    case XK_Alt_R:          return SW_KEY_RIGHT_ALT;
    }
    return SW_KEY_UNKNOWN;
}

void
sw_pump_events_internal(void)
{
    int         num_events = XPending(_xdisplay);
    XEvent      xev;
    sw_event_t  ev;
    for (int i = 0; i < num_events; ++i)
    {
        XNextEvent(_xdisplay, &xev);
        switch (xev.type)
        {
        case Expose:
            break;
        case KeyPress:
        {
            KeySym ks = XkbKeycodeToKeysym(_xdisplay, xev.xkey.keycode, 0, 0);
            ev.type         = SW_EVENT_KEY_DOWN;
            ev.key.keycode  = _x11_key_to_sw_key(ks);
            sw_post_event(&ev);
        }
            break;
        case KeyRelease:
            break;
        case ButtonPress:
            break;
        case ButtonRelease:
            break;
        case DestroyNotify:
            ev.type = SW_EVENT_QUIT;
            sw_post_event(&ev);
            break;
        case MotionNotify:
            break;
        case ConfigureNotify:
            ev.type     = SW_EVENT_WINDOW_SIZE_CHANGED;
            ev.window.w = xev.xconfigure.width;
            ev.window.h = xev.xconfigure.height;
            sw_post_window_event(&ev);
            break;
        case ClientMessage:
            if (xev.xclient.data.l[0] != _window->data->wm_delete_msg)
                break;
            ev.type = SW_EVENT_QUIT;
            sw_post_event(&ev);
            break;
        }
    }
}

int
sw_plat_framebuffer_data_init(sw_framebuffer_data_t *data, sw_window_t *window,
    int w, int h)
{
    int num_alloc_pixels = w * h;

    if (num_alloc_pixels < SW_X11_FRAMEBUFFER_MIN_ALLOC_PIXELS)
        num_alloc_pixels = SW_X11_FRAMEBUFFER_MIN_ALLOC_PIXELS;

    if (!(data->mem = sw_calloc(num_alloc_pixels * 4))) {
        sw_set_error(_sw_err_str_out_of_memory);
        return 1;
    }

    data->ximage = XCreateImage(_xdisplay, window->data->xvi.visual,
        window->data->xvi.depth, ZPixmap, 0, (char*)data->mem,
        w, h, 32, w * 4);

    if (!data->ximage) {
        sw_set_error("XCreateImage() failed");
        goto fail;
    }

    data->gc                = XCreateGC(_xdisplay, window->data->xwin, 0, 0);
    data->num_alloc_pixels  = num_alloc_pixels;
    data->xvi               = &window->data->xvi;

    return 0;

    fail:
        {return 2;}
}

void
sw_plat_destroy_framebuffer_data(sw_framebuffer_data_t *data)
{
    if (data->ximage)
        XDestroyImage(data->ximage);
    if (data->gc)
        XFreeGC(_xdisplay, data->gc);
    data->ximage    = 0;
    data->mem       = 0;
    data->gc        = 0;
}

void
sw_plat_update_framebuffer(sw_framebuffer_t *fb)
{
    sw_window_t *win = _window;
    XPutImage(_xdisplay, win->data->xwin, fb->data->gc, fb->data->ximage, 0,
        0, 0, 0, win->w, win->h);
    XSync(_xdisplay, False);
}

void *
sw_plat_get_framebuffer_data_pixels(sw_framebuffer_data_t *data)
    {return data->mem;}

int
sw_resize_framebuffer_data(sw_framebuffer_data_t *data, int w, int h)
{
    int num_alloc_pixels = w * h;

    data->ximage->data = 0;
    XDestroyImage(data->ximage);
    data->ximage = 0;

    if (num_alloc_pixels > data->num_alloc_pixels)
    {
        /* Grow at least by 50% */
        int num_req_pixels = data->ximage->width * data->ximage->height *
            150 / 100;
        if (num_req_pixels > num_alloc_pixels)
            num_alloc_pixels = num_req_pixels;

        void *mem = sw_realloc(data->mem, num_alloc_pixels * 4);
        if (!mem) {
            sw_set_error(_sw_err_str_out_of_memory);
            return 1;
        }

        data->mem               = mem;
        data->num_alloc_pixels  = num_alloc_pixels;
    }

    data->ximage = XCreateImage(_xdisplay, data->xvi->visual,
        data->xvi->depth, ZPixmap, 0, (char*)data->mem, w, h, 32, w * 4);
    if (!data->ximage) {
        sw_free(data->mem);
        data->mem = 0;
        return 2;
    }

    return 0;
}


long unsigned int
sw_plat_get_performance_frequency()
    {return 1000;}

long unsigned int
sw_plat_get_performance_counter()
{
    struct timespec ts;
    if (clock_gettime(CLOCK_MONOTONIC, &ts))
        return -1;
    long unsigned int sec   = ts.tv_sec;
    long unsigned int msec  = ts.tv_nsec / 1000000;

    long unsigned int prog_start_msec = _program_start_time.sec * 1000 +
        _program_start_time.msec;
    long unsigned int now_msec = sec * 1000 + msec;
    return now_msec - prog_start_msec;
}

int
sw_plat_sleep_milliseconds(long unsigned int num_ms)
{
    struct timespec req;
    req.tv_sec  = (time_t)num_ms / (time_t)1000;
    req.tv_nsec = ((long)num_ms - (long)req.tv_sec * (long)1000) * 1000000;
    return nanosleep(&req, &req);
}

int
sw_plat_init_time()
{
    struct timespec ts;
    if (clock_gettime(CLOCK_MONOTONIC, &ts))
        return 1;
    _program_start_time.sec   = ts.tv_sec;
    _program_start_time.msec  = ts.tv_nsec / 1000000;
    return 0;
}

void
sw_plat_quit_time(void) {}
