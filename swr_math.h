#ifndef SWR_MATH_H
#define SWR_MATH_H

#include <math.h>
#include <string.h>
#include "swr_common.h"

typedef float v3_t[3];
typedef float v4_t[4];
typedef float m4x4_t[4][4];

static inline void
m4x4_identity(m4x4_t r)
{
    r[0][0] = 1.f; r[0][1] = 0.f; r[0][2] = 0.f; r[0][3] = 0.f;
    r[1][0] = 0.f; r[1][1] = 1.f; r[1][2] = 0.f; r[1][3] = 0.f;
    r[2][0] = 0.f; r[2][1] = 0.f; r[2][2] = 1.f; r[2][3] = 0.f;
    r[3][0] = 0.f; r[3][1] = 0.f; r[3][2] = 0.f; r[3][3] = 1.f;
}

static inline void
m4x4_copy(m4x4_t r, m4x4_t m)
{
    int i, j;
    for (i = 0; i < 4; ++i)
        for (j = 0; j < 4; ++j)
            r[i][j] = m[i][j];
}

static inline void
m4x4_mul(m4x4_t r, m4x4_t a, m4x4_t b)
{
    m4x4_t tmp;
    int i, j, k;
    for (i = 0; i < 4; ++i)
        for (j = 0; j < 4; ++j) {
            tmp[i][j] = 0.f;
            for (k = 0; k < 4; ++k)
                tmp[i][j] += a[k][j] * b[i][k];
        }
    m4x4_copy(r, tmp);
}

static inline void
m4x4_rot_x(m4x4_t r, m4x4_t a, float rad)
{
    float s = sinf(rad);
    float c = cosf(rad);
    m4x4_t b = {
        {1.f, 0.f, 0.f, 0.f},
        {0.f,   c,  -s, 0.f},
        {0.f,   s,   c, 0.f},
        {0.f, 0.f, 0.f, 1.f}
    };
    m4x4_mul(r, a, b);
}

static inline void
m4x4_rot_y(m4x4_t r, m4x4_t a, float rad)
{
    float s = sinf(rad);
    float c = cosf(rad);
    m4x4_t b = {
        {c,   0.f,   s, 0.f},
        {0.f, 1.f, 0.f, 0.f},
        {-s,  0.f,   c, 0.f},
        {0.f, 0.f, 0.f, 1.f}
    };
    m4x4_mul(r, a, b);
}

static inline void
m4x4_rot_z(m4x4_t r, m4x4_t a, float rad)
{
    float s = sinf(rad);
    float c = cosf(rad);
    m4x4_t b = {
        {c,   -s,  0.f, 0.f},
        {s,    c,  0.f, 0.f},
        {0.f, 0.f, 1.f, 0.f},
        {0.f, 0.f, 0.f, 1.f}
    };
    m4x4_mul(r, a, b);
}

static inline void
v3_copy(v3_t r, v3_t v)
{
    for (int i = 0; i < 3; ++i)
        r[i] = v[i];
}

static inline void
v4_copy(v4_t r, v4_t v)
{
    for (int i = 0; i < 4; ++i)
        r[i] = v[i];
}

static inline void
v4_mul_mat4x4(v4_t r, v4_t v, m4x4_t m)
{
    v4_t tmp = {0};
    int i, j;
    for (i = 0; i < 4; ++i)
        for (j = 0; j < 4; ++j)
            tmp[i] += v[j] * m[j][i];
    v4_copy(r, tmp);
}

static inline void
m4x4_ortho(m4x4_t m, float l, float r, float b, float t, float n, float f)
{
    float rpl = r - l;
    float rml = r + l;
    float tpb = t + b;
    float tmb = t - b;
    float fpn = f + n;
    float fmn = f - n;

    float tx = rpl / rml;
    float ty = tpb / tmb;
    float tz = fpn / fmn;

    m[0][0] = 2.f / rml; m[0][1] = 0.f;     m[0][2] = 0.f;        m[0][3] = -tx;
    m[1][0] = 0.f;       m[1][1] = 2 / tmb; m[1][2] = 0.f;        m[1][3] = -ty;
    m[2][0] = 0.f;       m[2][1] = 0.f;     m[2][2] = -2.f / fmn; m[2][3] = -tz;
    m[3][0] = 0.f;       m[3][1] = 0.f;     m[3][2] = 0.f;        m[3][3] = 1.f;
}

static inline void
m4x4_perspective(m4x4_t m, float fov, float asp, float near, float far)
{
    swr_assert(fov > 0.f && asp != 0.f);

    //memset(m, 0, sizeof(m4x4_t));
    m4x4_identity(m);

    float frustum_depth = far - near;
    float one_over_depth = 1.f / frustum_depth;

    m[1][1] = 1.f / tanf(0.5f * fov);
    m[0][0] = m[1][1] / asp;
    m[2][2] = far * one_over_depth;
    m[3][2] = (-far * near) * one_over_depth;
    m[2][3] = 1.f;
    m[3][3] = 0.f;

#if 0
	float a = 1.f / tan(fov * 0.5f);

	m[0][0] = a / asp;
	m[0][1] = 0.f;
	m[0][2] = 0.f;
	m[0][3] = 0.f;

	m[1][0] = 0.f;
	m[1][1] = a;
	m[1][2] = 0.f;
	m[1][3] = 0.f;

	m[2][0] = 0.f;
	m[2][1] = 0.f;
	m[2][2] = -((far + near) / (far - near));
	m[2][3] = -1.f;

	m[3][0] = 0.f;
	m[3][1] = 0.f;
	m[3][2] = -((2.f * far * near) / (far - near));
    m[3][3] = 0.f;
#endif
}

static inline float
m4x4_det(m4x4_t m)
{
    float r = 0.f;
    r += m[0][0] * (m[1][1] * m[2][2] * m[3][3] - m[3][1] * m[2][2] * m[1][3]);
    r -= m[0][1] * (m[1][0] * m[2][2] * m[3][3] - m[3][0] * m[2][2] * m[1][3]);
    r += m[0][2] * (m[1][0] * m[2][1] * m[3][3] - m[3][0] * m[2][1] * m[1][3]);
    r -= m[0][3] * (m[1][0] * m[2][1] * m[3][2] - m[3][0] * m[2][1] * m[1][2]);
    r += m[1][0] * (m[0][1] * m[2][2] * m[3][3] - m[3][1] * m[2][2] * m[0][3]);
    r -= m[1][1] * (m[0][0] * m[2][2] * m[3][3] - m[3][0] * m[2][2] * m[0][3]);
    r += m[1][2] * (m[0][0] * m[2][1] * m[3][3] - m[3][0] * m[2][1] * m[0][3]);
    r -= m[1][3] * (m[0][0] * m[2][1] * m[3][2] - m[3][0] * m[2][1] * m[0][2]);
    r += m[2][0] * (m[0][1] * m[1][2] * m[3][3] - m[3][1] * m[1][2] * m[0][3]);
    r -= m[2][1] * (m[0][0] * m[1][2] * m[3][3] - m[3][0] * m[1][2] * m[0][3]);
    r += m[2][2] * (m[0][0] * m[1][2] * m[3][3] - m[3][0] * m[1][1] * m[0][3]);
    r -= m[2][3] * (m[0][0] * m[1][1] * m[3][2] - m[3][0] * m[1][1] * m[0][2]);
    r += m[3][0] * (m[0][1] * m[1][2] * m[2][3] - m[2][1] * m[1][2] * m[0][3]);
    r -= m[3][1] * (m[0][0] * m[1][2] * m[2][3] - m[2][0] * m[1][2] * m[0][3]);
    r += m[3][2] * (m[0][0] * m[1][1] * m[2][3] - m[2][0] * m[1][1] * m[0][3]);
    return r;
}

static inline void
m4x4_inv(m4x4_t r, m4x4_t m_in)
{
    /* From mesa3d.org */
    float *m = &m_in[0][0];
    float t[16], d;

    t[0] = m[5]  * m[10] * m[15] - 
        m[5]  * m[11] * m[14] - 
        m[9]  * m[6]  * m[15] + 
        m[9]  * m[7]  * m[14] +
        m[13] * m[6]  * m[11] - 
        m[13] * m[7]  * m[10];

    t[4] = -m[4]  * m[10] * m[15] + 
        m[4]  * m[11] * m[14] + 
        m[8]  * m[6]  * m[15] - 
        m[8]  * m[7]  * m[14] - 
        m[12] * m[6]  * m[11] + 
        m[12] * m[7]  * m[10];

    t[8] = m[4]  * m[9] * m[15] - 
             m[4]  * m[11] * m[13] - 
             m[8]  * m[5] * m[15] + 
             m[8]  * m[7] * m[13] + 
             m[12] * m[5] * m[11] - 
             m[12] * m[7] * m[9];

    t[12] = -m[4]  * m[9] * m[14] + 
               m[4]  * m[10] * m[13] +
               m[8]  * m[5] * m[14] - 
               m[8]  * m[6] * m[13] - 
               m[12] * m[5] * m[10] + 
               m[12] * m[6] * m[9];

    t[1] = -m[1]  * m[10] * m[15] + 
              m[1]  * m[11] * m[14] + 
              m[9]  * m[2] * m[15] - 
              m[9]  * m[3] * m[14] - 
              m[13] * m[2] * m[11] + 
              m[13] * m[3] * m[10];

    t[5] = m[0]  * m[10] * m[15] - 
             m[0]  * m[11] * m[14] - 
             m[8]  * m[2] * m[15] + 
             m[8]  * m[3] * m[14] + 
             m[12] * m[2] * m[11] - 
             m[12] * m[3] * m[10];

    t[9] = -m[0]  * m[9] * m[15] + 
              m[0]  * m[11] * m[13] + 
              m[8]  * m[1] * m[15] - 
              m[8]  * m[3] * m[13] - 
              m[12] * m[1] * m[11] + 
              m[12] * m[3] * m[9];

    t[13] = m[0]  * m[9] * m[14] - 
              m[0]  * m[10] * m[13] - 
              m[8]  * m[1] * m[14] + 
              m[8]  * m[2] * m[13] + 
              m[12] * m[1] * m[10] - 
              m[12] * m[2] * m[9];

    t[2] = m[1]  * m[6] * m[15] - 
             m[1]  * m[7] * m[14] - 
             m[5]  * m[2] * m[15] + 
             m[5]  * m[3] * m[14] + 
             m[13] * m[2] * m[7] - 
             m[13] * m[3] * m[6];

    t[6] = -m[0]  * m[6] * m[15] + 
              m[0]  * m[7] * m[14] + 
              m[4]  * m[2] * m[15] - 
              m[4]  * m[3] * m[14] - 
              m[12] * m[2] * m[7] + 
              m[12] * m[3] * m[6];

    t[10] = m[0]  * m[5] * m[15] - 
              m[0]  * m[7] * m[13] - 
              m[4]  * m[1] * m[15] + 
              m[4]  * m[3] * m[13] + 
              m[12] * m[1] * m[7] - 
              m[12] * m[3] * m[5];

    t[14] = -m[0]  * m[5] * m[14] + 
               m[0]  * m[6] * m[13] + 
               m[4]  * m[1] * m[14] - 
               m[4]  * m[2] * m[13] - 
               m[12] * m[1] * m[6] + 
               m[12] * m[2] * m[5];

    t[3] = -m[1] * m[6] * m[11] + 
              m[1] * m[7] * m[10] + 
              m[5] * m[2] * m[11] - 
              m[5] * m[3] * m[10] - 
              m[9] * m[2] * m[7] + 
              m[9] * m[3] * m[6];

    t[7] = m[0] * m[6] * m[11] - 
             m[0] * m[7] * m[10] - 
             m[4] * m[2] * m[11] + 
             m[4] * m[3] * m[10] + 
             m[8] * m[2] * m[7] - 
             m[8] * m[3] * m[6];

    t[11] = -m[0] * m[5] * m[11] + 
               m[0] * m[7] * m[9] + 
               m[4] * m[1] * m[11] - 
               m[4] * m[3] * m[9] - 
               m[8] * m[1] * m[7] + 
               m[8] * m[3] * m[5];

    t[15] = m[0] * m[5] * m[10] - 
              m[0] * m[6] * m[9] - 
              m[4] * m[1] * m[10] + 
              m[4] * m[2] * m[9] + 
              m[8] * m[1] * m[6] - 
              m[8] * m[2] * m[5];


    d = m[0] * t[0] + m[1] * t[4] + m[2] * t[8] + m[3] * t[12];
    d = 1.f / d;
    //d = 1.f / m4x4_det((void*)t);
    for (int i = 0; i < 16; ++i)
        *(float*)(void*)r = d * t[i];
}

static inline float
v3_len(v3_t v)
    {return sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);}

static inline void
v3_sub(v3_t r, v3_t a, v3_t b)
{
    r[0] = a[0] - b[0];
    r[1] = a[1] - b[1];
    r[2] = a[2] - b[2];
}

static inline void
v3_cross(v3_t r, v3_t a, v3_t b)
{
    v3_t tmp;
    tmp[0] = a[1] * b[2] + a[2] * b[1];
    tmp[1] = a[0] * b[2] + a[2] * b[0];
    tmp[2] = a[0] * b[1] + a[1] * b[0];
    v3_copy(r, tmp);
}

static inline void
v3_norm(v3_t r, v3_t v)
{
    float fs  = 1.f / v3_len(v);
    for (int i = 0; i < 3; ++i)
        r[i]  = fs * v[i];
}

static inline float
v3_dot(v3_t a, v3_t b)
    {return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];}

static inline float
v4_dot(v3_t a, v3_t b)
    {return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];}

static inline void
_print_mat4x4(m4x4_t m)
{
    for (int i = 0; i < 4; ++i) {
        printf("%f", m[i][0]);
        for (int j = 1; j < 4; ++j)
            printf(",\t%f", m[i][j]);
        puts("");
    }
}

static inline void
m4x4_look_at(m4x4_t r, v3_t e, v3_t c, v3_t u)
{
    v3_t vz, vx, vy;

    v3_sub(vz, e, c);
    v3_norm(vz, vz);

    v3_cross(vx, u, vz);
    v3_norm(vx, vx);

    v3_cross(vy, vz, vx);

#if 0
    m4x4_t inv = {
        {vx[0], vx[1], vx[2], 0.f},
        {vy[0], vy[1], vy[2], 0.f},
        {vz[0], vz[1], vz[2], 0.f},
        { -e[0], -e[1], -e[2], 1.f}
    };
#endif

    v3_t ne;
    for (int i = 0; i < 3; ++i)
        ne[i] = e[i] * -1.f;

    m4x4_t inv = {
        {vx[0], vy[0], vz[0], 0.f},
        {vx[1], vy[1], vz[1], 0.f},
        {vx[2], vy[2], vz[2], 0.f},
        {v3_dot(vx, ne), v3_dot(vy, ne), v3_dot(vz, ne), 1.f}
    };

    m4x4_copy(r, inv);
    //m4x4_inv(r, inv);
}

static inline void
m4x4_translate(m4x4_t r, float x, float y, float z)
{
    m4x4_identity(r);
    r[3][0] = x;
    r[3][1] = y;
    r[3][2] = z;
}

#endif /* SWR_MATH_H */
