#include "swr.h"
#include "sw.h"
#include "swr_common.h"
#include <stdlib.h>
#include <string.h>

struct swr_context_t {
    void    *pixels;
    int     w, h;
    int     num_alloc_pixels;
};

static inline void
_clamp_point(int *x, int *y, int min_x, int max_x, int min_y, int max_y)
{
    if (*x < min_x)
        *x = min_x;
    else if (*x > max_x)
        *x = max_x;
    if (*y < min_y)
        *y = min_y;
    else if (*y > max_y)
        *y = max_y;
}

static inline void
_compute_uv(v3_t p, v3_t iva, v3_t ivb, v3_t ivc, float *u, float *v, float *w)
{
    v3_t v0, v1, v2;
    v3_sub(v0, ivb, iva);
    v3_sub(v1, ivc, iva);
    v3_sub(v2, p, iva);

    float d00   = v3_dot(v0, v0);
    float d01   = v3_dot(v0, v1);
    float d11   = v3_dot(v1, v1);
    float d20   = v3_dot(v2, v0);
    float d21   = v3_dot(v2, v1);
    float denom = d00 * d11 - d01 * d01;

    *v = (d11 * d20 - d01 * d21) / denom;
    *w = (d00 * d21 - d01 * d20) / denom;
    *u = 1.0f - *v - *w;
}

swr_context_t *
swr_create_context(int w, int h)
{
    int num_pixels = w * h;
    if (!num_pixels)
        return 0;

    swr_context_t *ret = calloc(1, sizeof(swr_context_t));
    if (!ret)
        return 0;
    ret->pixels = calloc(1, num_pixels * 4);
    if (!ret->pixels) {
        free(ret);
        return 0;
    }

    ret->w                  = w;
    ret->h                  = h;
    ret->num_alloc_pixels   = num_pixels;
    return ret;
}

void
swr_destroy_context(swr_context_t *ctx)
{
    if (!ctx)
        return;
    free(ctx->pixels);
    free(ctx);
}

int
swr_resize(swr_context_t *context, int w, int h)
{
    int wh = w * h;
    if (wh > context->num_alloc_pixels) {
        int num_pixels = (context->w * context->h) * 150 / 100;
        if (wh > num_pixels)
            num_pixels = wh;
        void *pixels = realloc(context->pixels, num_pixels * 4);
        if (!pixels)
            return 1;
        context->pixels = pixels;
        context->num_alloc_pixels = num_pixels;
    }
    context->w = w;
    context->h = h;
    return 0;
}

void
swr_get_size(swr_context_t *context, int *ret_w, int *ret_h)
{
    *ret_w = context->w;
    *ret_h = context->h;
}

int
swr_swap_buffers(swr_context_t *context)
{
    int     w, h;
    void    *pixels = sw_get_window_framebuffer_pixels(&w, &h);
    if (!pixels)
        return 1;

    int fw = w < context-> w ? w : context->w;
    int fh = h < context-> h ? h : context->h;
    void *dst, *src;

    for (int i = 0; i < fh; ++i) {
        dst = (uint32_t*)pixels + i * w;
        src = (uint32_t*)context->pixels + i * context->w;
        memcpy(dst, src, fw * 4);
    }

    sw_update_window();
    return 0;
}

void
swr_line_2d(swr_context_t *context, int x1, int y1, int x2, int y2,
    uint32_t color)
{
    uint32_t *pixel;

    if (x1 != x2) {
        int tx1, ty1, tx2, ty2;

        if (x2 < x1) {
            tx1 = x2;
            ty1 = y2;
            tx2 = x1;
            ty2 = y1;
        } else {
            tx1 = x1;
            ty1 = y1;
            tx2 = x2;
            ty2 = y2;
        }

        _clamp_point(&tx1, &ty1, 0, context->w - 1, 0, context->h - 1);
        _clamp_point(&tx2, &ty2, 0, context->w - 1, 0, context->h - 1);

        int dx = x2 - x1;
        int dy = y2 - y1;
        int y;

        for (int x = tx1; x < tx2; ++x) {
            y = ty1 + dy * (x - tx1) / dx;
            pixel = (uint32_t*)context->pixels + (y * context->w + x);
            *pixel = color;
        }
    } else {
        for (int y = y1; y <= y2; ++y) {
            pixel = (uint32_t*)context->pixels + (y * context->w + x1);
            *pixel = color;
        }
    }
}

void
swr_line_bresenham_2d(swr_context_t *context, int x1, int y1, int x2, int y2,
    uint32_t color)
{
    if (x2 < x1)
    {
        int tmp;
        tmp = x1;
        x1  = x2;
        x2  = tmp;
        tmp = y1;
        y1  = y2;
        y2  = tmp;
    }

    char        *pixels = context->pixels;
    uint32_t    *pixel;
    int         cw = context->w;
    int         ch = context->h;

    uint32_t *last_pixel = (uint32_t*)context->pixels + cw * ch;

    if (x1 != x2) {
        float   dx  = (float)(x2 - x1);
        float   dy  = (float)(y2 - y1);
        float   dd  = dy / dx;
        float   de  = (float)fabs(dd);
        float   e   = 0.f;
        int     y   = y1;
        int     dys = dy > 0 ? 1 : -1;

        for (int x = x1; x <= x2; ++x) {
            pixel   = (uint32_t*)pixels + (y * cw + x);
            if (pixel >= (uint32_t*)context->pixels && pixel < last_pixel)
                *pixel  = color;
            e += de;
            while (e >= 0.5f) {
                y = y + dys;
                e -= 1.f;
            }
        }
    } else {
        for (int y = y1; y <= y2; ++y) {
            pixel = (uint32_t*)context->pixels + (y * context->w + x1);
            if (pixel >= (uint32_t*)context->pixels && pixel < last_pixel)
                *pixel = color;
        }
    }
}

void
swr_fill_rect_2d(swr_context_t *context, int x, int y, int w, int h,
    uint32_t color)
{
    int x1 = x;
    int y1 = y;
    int x2 = x + w;
    int y2 = y + h;

    _clamp_point(&x1, &y1, 0, context->w - 1, 0, context->h - 1);
    _clamp_point(&x2, &y2, 0, context->w - 1, 0, context->h - 1);

    int         tw = x2 - x1;
    int         ix, iy;
    uint32_t    *pixel;

    for (ix = x1; ix < x2; ++ix)
        for (iy = y1; iy < y2; ++iy) {
            pixel = (uint32_t*)context->pixels + iy * tw + ix;
            *pixel = color;
        }
}

void
swr_fill_triangle_2d_top(swr_context_t *context, int x1, int y1, int x2, int y2,
    int x3, int y3, uint32_t color)
{
    int v1[2]   = {x1, y1};
    int v2[2]   = {x2, y2};
    int v3[2]   = {x3, y3};
    int *sv[3]  = {v1, v2, v3};

    swr_assert(sv[0][1] <= sv[1][1] && sv[0][1] <= sv[2][1]);
    swr_assert(sv[0][1] <= sv[1][1] && sv[0][1] <= sv[1][1]);

    if (sv[2][0] < sv[1][0]) {
        int *tmp = sv[2];
        sv[2] = sv[1];
        sv[1] = tmp;
    }

    float il = (float)(sv[1][0] - sv[0][0]) / (float)(sv[1][1] - sv[0][1]);
    float ir = (float)(sv[2][0] - sv[0][0]) / (float)(sv[2][1] - sv[0][1]);

    float xl = (float)sv[0][0];
    float xr = (float)sv[0][0];

    int         x, y, txl, txr;
    uint32_t    *pixel;

    /* Clamp values on the screen */

    if (sv[0][1] < 0) {
        xl += il * (-1 * sv[0][1]);
        xr += ir * (-1 * sv[0][1]);
        sv[0][1] = 0;
    }

    if (sv[1][1] >= context->h)
        sv[1][1] = context->h - 1;

    for (y = sv[0][1]; y <= sv[1][1]; ++y) {
        txl = (int)xl;
        txr = (int)xr;
        if (txl < 0)
            txl = 0;
        if (txr >= context->w)
            txr = context->w - 1;
        for (x = txl; x <= txr; ++x) {
            pixel = (uint32_t*)context->pixels + y * context->w + x;
            *pixel = color;
        }
        xl += il;
        xr += ir;
    }
}

void
swr_fill_triangle_2d_bottom(swr_context_t *context, int x1, int y1,
    int x2, int y2, int x3, int y3, uint32_t color)
{
    int v1[2]   = {x1, y1};
    int v2[2]   = {x2, y2};
    int v3[2]   = {x3, y3};
    int *sv[3]  = {v1, v2, v3};

    int swapped = 1;

    while (swapped) {
        swapped = 0;
        for (int i = 0; i < 2; ++i) {
            if (sv[i][1] <= sv[i + 1][1])
                continue;
            int *vtmp = sv[i];
            sv[i]       = sv[i + 1];
            sv[i + 1]   = vtmp;
            swapped     = 1;
        }
    }

    if (sv[1][0] < sv[0][0]) {
        int *tmp = sv[0];
        sv[0] = sv[1];
        sv[1] = tmp;
    }

    swr_assert(sv[2][1] >= sv[0][1] && sv[2][1] >= sv[1][1]);
    swr_assert(sv[0][0] <= sv[1][0]);

    float il = (float)(sv[2][0] - sv[0][0]) / (float)(sv[2][1] - sv[0][1]);
    float ir = (float)(sv[2][0] - sv[1][0]) / (float)(sv[2][1] - sv[1][1]);

    float xl = (float)sv[2][0];
    float xr = (float)sv[2][0];

    int         x, y, txl, txr;
    uint32_t    *pixel;

    /* Clamp values on the screen */

    if (sv[2][1] >= context->h) /* y starting point */ {
        xl -= (sv[2][1] - (context->h - 1)) * il;
        xr -= (sv[2][1] - (context->h - 1)) * ir;
        sv[2][1] = context->h - 1;
    }

    if (sv[0][1] < 0) /* y end point */
        sv[0][1] = 0;

    for (y = sv[2][1]; y >= sv[0][1]; --y) {
        txl = (int)xl;
        txr = (int)xr;
        if (txl < 0)
            txl = 0;
        if (txr >= context->w)
            txr = context->w - 1;
        for (x = txl; x <= txr; ++x) {
            pixel = (uint32_t*)context->pixels + y * context->w + x;
            *pixel = color;
        }
        xl -= il;
        xr -= ir;
    }
}

void
swr_fill_triangle_2d(swr_context_t *context, int x1, int y1, int x2, int y2,
    int x3, int y3, uint32_t color)
{
    if (y1 == y2 && y2 == y3)
        return;

    int v1[2]   = {x1, y1};
    int v2[2]   = {x2, y2};
    int v3[2]   = {x3, y3};
    int *sv[3]  = {v1, v2, v3};

    int swapped = 1;

    while (swapped) {
        swapped = 0;
        for (int i = 0; i < 2; ++i) {
            if (sv[i][1] <= sv[i + 1][1])
                continue;
            int *vtmp = sv[i];
            sv[i]       = sv[i + 1];
            sv[i + 1]   = vtmp;
            swapped     = 1;
        }
    }

    float   val = (float)(sv[2][1] - sv[0][1]);
    int     v4[2];

    v4[0] = (int)(sv[0][0] + ((float)(sv[1][1] - sv[0][1]) / (float)(sv[2][1] -
        sv[0][1])) * (sv[2][0] - sv[0][0]));
    v4[1] = sv[1][1];

    swr_fill_triangle_2d_top(context, sv[0][0], sv[0][1], sv[1][0],
        sv[1][1], v4[0], v4[1], color);
    swr_fill_triangle_2d_bottom(context, sv[1][0], sv[1][1], v4[0],
        v4[1], sv[2][0], sv[2][1], color);
}

void
swr_line_mesh(swr_context_t *context, m4x4_t mvp, swr_mesh_t *mesh,
    uint32_t color)
{
    float       *vertices       = mesh->vertices;
    uint32_t    num_triangles   = mesh->num_vertices / 3;
    float       ctx_half_w      = (float)(context->w / 2);
    float       ctx_half_h      = (float)(context->h / 2);
    v4_t        vt1, vt2;
    float       *triangle, *v1, *v2;
    int         x1, y1, x2, y2;

    if (!num_triangles)
        return;

    for (uint32_t i = 0; i < num_triangles; ++i)
    {
        triangle = vertices + i * 9;
        for (uint32_t j = 0; j < 3; ++j)
        {
            v1 = triangle + j * 3;
            v2 = triangle + (j + 1) % 3 * 3;
            v3_copy(vt1, v1);
            v3_copy(vt2, v2);
            vt1[3] = 1.f;
            vt2[3] = 1.f;

            v4_mul_mat4x4(vt1, vt1, mvp);
            v4_mul_mat4x4(vt2, vt2, mvp);

            x1 = (int)(vt1[0] / vt1[2] * ctx_half_w + ctx_half_w);
            y1 = (int)(vt1[1] / vt1[2] * ctx_half_h + ctx_half_h);
            x2 = (int)(vt2[0] / vt2[2] * ctx_half_w + ctx_half_w);
            y2 = (int)(vt2[1] / vt2[2] * ctx_half_h + ctx_half_h);

            swr_line_bresenham_2d(context, x1, y1, x2, y2, color);
        }
    }
}

void
swr_fill_mesh(swr_context_t *context, m4x4_t mvp, swr_mesh_t *mesh,
    uint32_t color)
{
    float       *vertices       = mesh->vertices;
    uint32_t    num_triangles   = mesh->num_vertices / 3;
    float       ctx_half_w      = (float)(context->w / 2);
    float       ctx_half_h      = (float)(context->h / 2);
    v4_t        vt1             = {0.f, 0.f, 0.f, 1.f};
    v4_t        vt2             = {0.f, 0.f, 0.f, 1.f};
    v4_t        vt3             = {0.f, 0.f, 0.f, 1.f};
    float       *triangle, *v1, *v2, *v3;
    int         x1, y1, x2, y2, x3, y3;

    if (!num_triangles)
        return;

    for (uint32_t i = 0; i < num_triangles; ++i) {
        triangle = vertices + i * 9;
        v1 = triangle;
        v2 = triangle + 3;
        v3 = triangle + 6;

        vt1[3] = 1.f;
        vt2[3] = 1.f;
        vt3[3] = 1.f;
        v3_copy(vt1, v1);
        v3_copy(vt2, v2);
        v3_copy(vt3, v3);

        v4_mul_mat4x4(vt1, vt1, mvp);
        v4_mul_mat4x4(vt2, vt2, mvp);
        v4_mul_mat4x4(vt3, vt3, mvp);

        x1 = (int)(vt1[0] / vt1[2] * ctx_half_w + ctx_half_w);
        y1 = (int)(vt1[1] / vt1[2] * ctx_half_h + ctx_half_h);
        x2 = (int)(vt2[0] / vt2[2] * ctx_half_w + ctx_half_w);
        y2 = (int)(vt2[1] / vt2[2] * ctx_half_h + ctx_half_h);
        x3 = (int)(vt3[0] / vt3[2] * ctx_half_w + ctx_half_w);
        y3 = (int)(vt3[1] / vt3[2] * ctx_half_h + ctx_half_h);

        swr_fill_triangle_2d(context, x1, y1, x2, y2, x3, y3, color);
    }

}
