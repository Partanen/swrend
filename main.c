#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <inttypes.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "sw.h"
#include "swr.h"
#include "swr_math.h"

#ifndef M_PI
    #define M_PI 3.141592653589793
#endif

static inline double
_compute_delta(long unsigned int tick_time, long unsigned int last_tick,
    long unsigned int performance_frequency, double target_delta);

static void
_render(swr_context_t *context);

static inline float
sine_wave(int frame)
    {return 2 * (float)sin((double)frame * 0.25 * M_PI / 3);}

static float _cube_verts[] = {
    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f,  0.5f, -0.5f,
    0.5f,  0.5f, -0.5f,
    -0.5f,  0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,

    -0.5f, -0.5f,  0.5f,
    0.5f, -0.5f,  0.5f,
    0.5f,  0.5f,  0.5f,
    0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f,  0.5f,
    -0.5f, -0.5f,  0.5f,

    -0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,
    -0.5f, -0.5f, -0.5f,
    -0.5f, -0.5f,  0.5f,
    -0.5f,  0.5f,  0.5f,

    0.5f,  0.5f,  0.5f,
    0.5f,  0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f,  0.5f,
    0.5f,  0.5f,  0.5f,

    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f,  0.5f,
    0.5f, -0.5f,  0.5f,
    -0.5f, -0.5f,  0.5f,
    -0.5f, -0.5f, -0.5f,

    -0.5f,  0.5f, -0.5f,
    0.5f,  0.5f, -0.5f,
    0.5f,  0.5f,  0.5f,
    0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f,  0.5f,
    -0.5f,  0.5f, -0.5f
};

static float _uv_cube_vertices[] = {
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
};

static sw_img_t chess_tex;

int main(int argc, char **argv)
{
    (void)argc; (void)argv;
    srand((int)time(0));

    int ret = 0;

    if (sw_init(0)) {
        ret = 1;
        goto fail;
    }

    if (sw_create_window("jee", 0, 0, 800, 600, 0)) {
        ret = 2;
        goto fail;
    }

    swr_context_t *swr_context = swr_create_context(800, 600);
    if (!swr_context) {
        ret = 3;
        goto fail;
    }

    int running = 1;

    long unsigned int   pf              = sw_get_performance_frequency();
    long unsigned int   last_tick       = 0;
    double              target_delta    = 1.f / 60.f;

    while (running) {
        sw_event_t ev;
        while (sw_poll_events(&ev)) {
            switch (ev.type) {
            case SW_EVENT_QUIT:
                running = 0;
                break;
            case SW_EVENT_KEY_DOWN:
                switch (ev.key.keycode) {
                    case SW_KEY_ESCAPE:
                        running = 0;
                        break;
                }
                break;
            }
        }

        if (!running)
            break;

        _render(swr_context);

        /*-- Tick the clock --*/
        long unsigned int   tick_time;
        double              delta;

        tick_time   = sw_get_performance_counter();
        delta       = _compute_delta(tick_time, last_tick, pf, target_delta);
        if (delta < target_delta) {
            long unsigned int s =
                (long unsigned int)((target_delta - delta) * 1000.f);
            sw_sleep_milliseconds(s);
            tick_time = sw_get_performance_counter();
            delta = _compute_delta(tick_time, last_tick, pf, target_delta);
        }
        last_tick = tick_time;
    }

    sw_quit();
    swr_destroy_context(swr_context);
    sw_img_free(&chess_tex);

    return ret;

    fail:
        puts(sw_get_error());
        return ret;
}

static inline double
_compute_delta(long unsigned int tick_time, long unsigned int last_tick,
    long unsigned int performance_frequency, double target_delta)
{
    double delta;
    if (last_tick)
        delta = (double)(tick_time - last_tick) / (double)performance_frequency;
    else
        delta = target_delta;
    return delta;
}

static void
_render(swr_context_t *context)
{
    static int      frame   = 0;
    static float    fov     = 90.f;
    static float    xx      = 0.f;
    xx += 0.01f;
    float floating = sine_wave(frame++);
    fov = 90.f + floating * 0.02f;

    static float rx = 0.f;
    static float ry = 0.f;
    static float rz = 0.f;
    rx += 0.002f;
    ry += 0.002f;
    rz -= 0.02f;

    int w, h;
    swr_get_size(context, &w, &h);
    swr_fill_rect_2d(context, 0, 0, w, h, 0);

    swr_mesh_t mesh;
    mesh.vertices       = _cube_verts;
    mesh.num_vertices   = sizeof(_cube_verts) / sizeof(float) / 3;

    m4x4_t mvp;
    m4x4_identity(mvp);

    /*-- Translate --*/
    m4x4_t translate;
    m4x4_translate(translate, 0.0f, floating * 0.07f, 0.f);
    //m4x4_rot_x(translate, translate, rx);
    //m4x4_rot_y(translate, translate, ry);
    //m4x4_rot_z(translate, translate, rz);
    m4x4_mul(mvp, mvp, translate);

    /*-- Project --*/
    m4x4_t persp;
    m4x4_perspective(persp, 90.f, (float)w / (float)h, -1.f, 300.f);
    m4x4_mul(mvp, mvp, persp);

    /*-- Look at --*/
    m4x4_t look_at;
    v3_t eye      = {0.2f, 0.0f, 3.0f};
    v3_t center   = {0.0f, 0.0f, 0.0f};
    v3_t up       = {0.0f, 1.0f, 0.0f};
    m4x4_look_at(look_at, eye, center, up);
    m4x4_rot_y(look_at, look_at, rx * 3.f);
    m4x4_rot_z(look_at, look_at, rz);
    m4x4_mul(mvp, mvp, look_at);

    static unsigned char mesh_color[4] = {255, 0, 255, 255};

    int da = 1;
    int db = -1;
    mesh_color[0] = (mesh_color[0] + da) % 255;
    mesh_color[2] = (mesh_color[2] + db) % 255;

    if (da == 0 || da == 255) {
        da *= -1;
        db *= -1;
    }

    swr_fill_mesh(context, mvp, &mesh, *(uint32_t*)mesh_color);
    //swr_line_mesh(context, mvp, &mesh, 0xFFFFFFFF);

    swr_swap_buffers(context);
}
