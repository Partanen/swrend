#include <stdint.h>
#include "swr_math.h"

#ifndef SWR_H
#define SWR_H

typedef struct swr_context_t    swr_context_t;
typedef struct swr_mesh_t       swr_mesh_t;

struct swr_mesh_t
{
    float       *vertices;
    uint32_t    num_vertices;
};

swr_context_t *
swr_create_context(int w, int h);

void
swr_destroy_context(swr_context_t *ctx);

int
swr_resize(swr_context_t *context, int w, int h);

void
swr_get_size(swr_context_t *context, int *ret_w, int *ret_h);

int
swr_swap_buffers(swr_context_t *context);

void
swr_line_2d(swr_context_t *context, int x1, int y1, int x2, int y2,
    uint32_t color);

void
swr_line_bresenham_2d(swr_context_t *context, int x1, int y1, int x2, int y2,
    uint32_t color);

void
swr_fill_rect_2d(swr_context_t *context, int x, int y, int w, int h,
    uint32_t color);

void
swr_fill_triangle_2d_top(swr_context_t *context, int x1, int y1, int x2, int y2,
    int x3, int y3, uint32_t color);

void
swr_fill_triangle_2d_bottom(swr_context_t *context, int x1, int y1,
    int x2, int y2, int x3, int y3, uint32_t color);

void
swr_fill_triangle_2d(swr_context_t *context, int x1, int y1, int x2, int y2,
    int x3, int y3, uint32_t color);

void
swr_line_mesh(swr_context_t *context, m4x4_t mvp, swr_mesh_t *mesh,
    uint32_t color);

void
swr_fill_mesh(swr_context_t *context, m4x4_t mvp, swr_mesh_t *mesh,
    uint32_t color);

#endif /* SWR_H */
